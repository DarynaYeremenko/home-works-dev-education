package com.github.oldHomeWork.test;

import com.github.oldHomeWork.main.Functions;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class FunctionsTests {

    //============================================
    //============= dayOfWeek ====================
    //============================================

    @Test
    public void testDayOfWeekSaturday() {
        assertEquals("Суббота", Functions.dayOfWeek(6));
    }

    @Test
    public void testDayOfWeekMonday() {
        assertEquals("Понедельник", Functions.dayOfWeek(1));
    }

    @Test
    public void testDayOfWeekSunday() {
        assertEquals("Воскресенье", Functions.dayOfWeek(7));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testDayOfWeekIfDayMoreThanMax() {
        Functions.dayOfWeek(8);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testDayOfWeekIfDayLessThanMin() {
        Functions.dayOfWeek(0);
    }


    //============================================
    //============= writeNumber ==================
    //============================================

    @Test
    public void testWriteNumberZero() {
        assertEquals("ноль", Functions.writeNumber(0));
    }

    @Test
    public void testWriteNumberTens() {
        assertEquals("тридцать четыре", Functions.writeNumber(34));
    }

    @Test
    public void testWriteNumberHundreds() {
        assertEquals("триста сорок семь", Functions.writeNumber(347));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testWriteNumberNegativeValue() {
        Functions.writeNumber(-347);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testWriteNumberMoreThanMaxValue() {
        Functions.writeNumber(100000);
    }

    //============================================
    //============= getNumber ====================
    //============================================

    @Test
    public void testGetNumberZero() {
        assertEquals(0, Functions.getNumber("ноль"));
    }

    @Test
    public void testGetNumberHundreds() {
        assertEquals(347, Functions.getNumber("триста сорок семь"));
    }

    @Test
    public void testGetNumberTens() {
        assertEquals(47, Functions.getNumber("сорок семь"));
    }

    @Test(expected = NullPointerException.class)
    public void testGetNumberWrongValue() {
        Functions.getNumber("число");
    }

    @Test(expected = NullPointerException.class)
    public void testGetNumberMoreThanMaxValue() {
        Functions.getNumber("миллион");
    }

    //============================================
    //============= getNumber ====================
    //============================================

    @Test
    public void testCartesianDistancePositiveValues() {
        assertEquals("6,40", Functions.cartesianDistance(1, 1, 5, 6));
    }

    @Test
    public void testCartesianDistanceNegativeValues() {
        assertEquals("17,46", Functions.cartesianDistance(-1, -10, 6, 6));
    }

    @Test
    public void testCartesianDistanceWithTheSameValues() {
        assertEquals("0", Functions.cartesianDistance(1, 1, 1, 1));
    }
}
