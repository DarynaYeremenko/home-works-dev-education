package com.github.oldHomeWork.test;

import com.github.oldHomeWork.main.ConditionalOperators;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ConditionalOperatorsTests {

    //============================================
    //============= multiplyEvenNumber ===========
    //============================================

    @Test
    public void testMultiplyEvenNumberIfOddNumber() {
        assertEquals(4, ConditionalOperators.multiplyEvenNumber(1, 3));
    }

    @Test
    public void testMultiplyEvenNumberIfEvenNumber() {
        assertEquals(16, ConditionalOperators.multiplyEvenNumber(4, 4));
    }

    //============================================
    //============= quarterDefine ================
    //============================================

    @Test
    public void testQuarterDefineForFourthQuarter() {
        assertEquals("4 четверть", ConditionalOperators.quarterDefine(1, -2));
    }

    @Test
    public void testQuarterDefinerForFirstQuarter() {
        assertEquals("Первая четверть", ConditionalOperators.quarterDefine(4, 10));
    }

    @Test
    public void testQuarterDefinerForThirdQuarter() {
        assertEquals("3 четверть", ConditionalOperators.quarterDefine(-4, -1));
    }

    @Test
    public void testQuarterDefinerForSecondQuarter() {
        assertEquals("2 четверть", ConditionalOperators.quarterDefine(-3, 10));
    }

    @Test
    public void testQuarterDefinerIfCanNotDetermined() {
        assertEquals("нельзя определить четверть для указанных координат", ConditionalOperators.quarterDefine(0, 0));
    }

    //============================================
    //============= positiveNumbersSum ===========
    //============================================

    @Test
    public void testPositiveNumbersSumForOnePosNum() {
        assertEquals(7, ConditionalOperators.positiveNumbersSum(-4, -1, 7));
    }

    @Test
    public void testPositiveNumbersSumForTwoPosNum() {
        assertEquals(3, ConditionalOperators.positiveNumbersSum(1, 2, -9));
    }

    @Test
    public void testPositiveNumbersSumForThreePosNum() {
        assertEquals(15, ConditionalOperators.positiveNumbersSum(4, 10, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPositiveNumbersSumIfNoPositiveNumbers() {
        ConditionalOperators.positiveNumbersSum(-3, -10, -9);
    }


    //============================================
    //============= positiveNumbersSum ===========
    //============================================


    @Test
    public void testMaxSumOrMultiplicationMaxIfSumMax() {
        assertEquals(6, ConditionalOperators.maxSumOrMultiplicationMax(1, 2, 0));
    }

    @Test
    public void testMaxSumOrMultiplicationMaxIfMultiplicationMax() {
        assertEquals(19, ConditionalOperators.maxSumOrMultiplicationMax(2, 2, 4));
    }

    @Test
    public void testMaxSumOrMultiplicationMaxIfResultEqual() {
        assertEquals(3, ConditionalOperators.maxSumOrMultiplicationMax(0, 0, 0));
    }

    //============================================
    //============= defineRating ===========
    //============================================

    @Test
    public void testDefineRatingLowestMark() {
        assertEquals("F", ConditionalOperators.defineRating(1));
    }

    @Test
    public void testDefineRatingMediumMark() {
        assertEquals("D", ConditionalOperators.defineRating(51));
    }

    @Test
    public void testDefineRatingMaxMark() {
        assertEquals("A", ConditionalOperators.defineRating(99));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDefineRatingLessZero() {
        ConditionalOperators.defineRating(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDefineRatingMoreThanMaxValue() {
        ConditionalOperators.defineRating(101);
    }
}
