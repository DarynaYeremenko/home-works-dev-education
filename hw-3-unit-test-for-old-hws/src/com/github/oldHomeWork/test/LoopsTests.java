package com.github.oldHomeWork.test;

import com.github.oldHomeWork.main.Loops;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class LoopsTests {

    //============================================
    //======== sumEvenNumbersAndCount ============
    //============================================

    @Test
    public void testSumEvenNumbersAndCount() {
        assertArrayEquals(new int[]{2450, 50}, Loops.sumEvenNumbersAndCount());
    }

    //============================================
    //============= isPrimeNumber ================
    //============================================

    @Test
    public void testIsPrimeNumberTrue() {
        assertTrue(Loops.isPrimeNumber(3));
    }

    @Test
    public void testIsPrimeNumberFalse() {
        assertFalse(Loops.isPrimeNumber(12));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsPrimeNumberZero() {
        Loops.isPrimeNumber(0);
    }

    //============================================
    //============== sqrtSequential ==============
    //============================================

    @Test
    public void testSqrtSequential() {
        assertEquals(5, Loops.sqrtSequential(25));
    }

    @Test
    public void testSqrtSequentialWithMinValue() {
        assertEquals(1, Loops.sqrtSequential(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSqrtSequentialWithNegativeValue() {
        Loops.sqrtSequential(-10);
    }

    //============================================
    //============== sqrtBinary ==================
    //============================================

    @Test
    public void testSqrtBinary() {
        assertEquals(5, Loops.sqrtBinary(25));
    }

    @Test
    public void testSqrtBinaryWithMinValue() {
        assertEquals(1, Loops.sqrtBinary(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSqrtBinaryWithNegativeValue() {
        Loops.sqrtBinary(-10);
    }

    //============================================
    //============== findFactorial ===============
    //============================================

    @Test
    public void testFindFactorial() {
        assertEquals(5040, Loops.findFactorial(7));
    }

    @Test
    public void testFindFactorialWithMinValue() {
        assertEquals(1, Loops.findFactorial(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindFactorialWithNegativeValue() {
        Loops.findFactorial(-9);
    }

    //============================================
    //============== sumNumbersOfValue ===========
    //============================================

    @Test
    public void testSumNumbersOfValue() {
        assertEquals(8, Loops.sumNumbersOfValue(71));
    }

    @Test
    public void testSumNumbersOfValueWithOneElement() {
        assertEquals(7, Loops.sumNumbersOfValue(7));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSumNumbersOfValueWithNegativeElement() {
        Loops.sumNumbersOfValue(-71);
    }

    //============================================
    //============== mirrorSequence ==============
    //============================================

    @Test
    public void testMirrorSequence() {
        assertEquals(321, Loops.mirrorSequence(123));
    }

    @Test
    public void testMirrorSequenceWithOneElement() {
        assertEquals(1, Loops.mirrorSequence(1));
    }
}
