package com.github.oldHomeWork.test;

import com.github.oldHomeWork.main.OneDimensionalArrays;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertArrayEquals;

public class OneDimensionalArraysTests {

    //============================================
    //============= arrayMin =====================
    //============================================

    @Test
    public void testArrayMin() {
        assertEquals(1, OneDimensionalArrays.arrayMin(new int[]{3, 1, 6}));
    }

    @Test
    public void testArrayMinWithTwoMinElements() {
        assertEquals(1, OneDimensionalArrays.arrayMin(new int[]{10, 11, 5, 1, 7, 1}));
    }

    @Test
    public void testArrayMinWithOneElement() {
        assertEquals(3, OneDimensionalArrays.arrayMin(new int[]{3}));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testArrayMinWithNoElements() {
        OneDimensionalArrays.arrayMin(new int[]{});
    }

    //============================================
    //============= arrayMax =====================
    //============================================

    @Test
    public void testArrayMax() {
        assertEquals(6, OneDimensionalArrays.arrayMax(new int[]{3, 1, 6}));
    }

    @Test
    public void testArrayMaxWithTwoMinElements() {
        assertEquals(11, OneDimensionalArrays.arrayMax(new int[]{10, 11, 5, 11, 7, 1}));
    }

    @Test
    public void testArrayMaxWithOneElement() {
        assertEquals(3, OneDimensionalArrays.arrayMax(new int[]{3}));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testArrayMaxWithNoElements() {
        OneDimensionalArrays.arrayMax(new int[]{});
    }

    //============================================
    //============= getIndexOfArrayMin ===========
    //============================================

    @Test
    public void testGetIndexOfArrayMin() {
        assertEquals(1, OneDimensionalArrays.getIndexOfArrayMin(new int[]{3, 1, 6}));
    }

    @Test
    public void testGetIndexOfArrayMinWithTwoMinElements() {
        assertEquals(5, OneDimensionalArrays.getIndexOfArrayMin(new int[]{10, 11, 5, 11, 7, 1}));
    }

    @Test
    public void testGetIndexOfArrayMinWithOneElement() {
        assertEquals(0, OneDimensionalArrays.getIndexOfArrayMin(new int[]{3}));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGetIndexOfArrayMinWithNoElements() {
        OneDimensionalArrays.getIndexOfArrayMin(new int[]{});
    }

    //============================================
    //============= getIndexOfArrayMin ===========
    //============================================

    @Test
    public void testGetIndexOfArrayMax() {
        assertEquals(2, OneDimensionalArrays.getIndexOfArrayMax(new int[]{3, 1, 6}));
    }

    @Test
    public void testGetIndexOfArrayMaxWithTwoMinElements() {
        assertEquals(1, OneDimensionalArrays.getIndexOfArrayMax(new int[]{10, 11, 5, 11, 7, 1}));
    }

    @Test
    public void testGetIndexOfArrayMaxWithOneElement() {
        assertEquals(0, OneDimensionalArrays.getIndexOfArrayMax(new int[]{3}));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGetIndexOfArrayMaxWithNoElements() {
        OneDimensionalArrays.getIndexOfArrayMax(new int[]{});
    }

    //============================================
    //============= sumOfArrayOdd ================
    //============================================

    @Test
    public void testSumOfArrayOdd() {
        assertEquals(9, OneDimensionalArrays.sumOfArrayOdd(new int[]{3, 1, 6, 8}));
    }

    @Test
    public void testSumOfArrayOddIfNoOddElements() {
        assertEquals(0, OneDimensionalArrays.sumOfArrayOdd(new int[]{12}));
    }

    @Test
    public void testSumOfArrayOddWithEmptyArray() {
        assertEquals(0, OneDimensionalArrays.sumOfArrayOdd(new int[]{12}));
    }

    //============================================
    //============= arrayRevers ==================
    //============================================

    @Test
    public void testArrayRevers() {
        assertArrayEquals(new int[]{8, 6, 1, 3}, OneDimensionalArrays.arrayRevers(new int[]{3, 1, 6, 8}));
    }

    @Test
    public void testArrayReversWithOneElement() {
        assertArrayEquals(new int[]{3}, OneDimensionalArrays.arrayRevers(new int[]{3}));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testArrayReversWithNoElements() {
        OneDimensionalArrays.arrayRevers(new int[]{});
    }

    //============================================
    //========= countOfArrayOddElements ==========
    //============================================

    @Test
    public void testCountOfArrayOddElements() {
        assertEquals(2, OneDimensionalArrays.countOfArrayOddElements(new int[]{3, 1, 6, 8}));
    }

    @Test
    public void testCountOfArrayOddElementsIfNoOddElements() {
        assertEquals(0, OneDimensionalArrays.countOfArrayOddElements(new int[]{4, 2, 6, 8}));
    }

    @Test
    public void testCountOfArrayOddElementsWithEmptyArray() {
        assertEquals(0, OneDimensionalArrays.countOfArrayOddElements(new int[0]));
    }

    //============================================
    //========= replaceTwoPartOfArray ============
    //============================================

    @Test
    public void testReplaceTwoPartOfArray() {
        assertArrayEquals(new int[]{4, 5, 6, 1, 2, 3}, OneDimensionalArrays.replaceTwoPartOfArray(new int[]{1, 2, 3, 4, 5, 6}));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testReplaceTwoPartOfArrayWithOneElement() {
        OneDimensionalArrays.replaceTwoPartOfArray(new int[]{4});
    }

    //============================================
    //============= bubbleSort ===================
    //============================================

    @Test
    public void testBubbleSort() {
        assertArrayEquals(new int[]{1, 3, 5, 7, 11, 12, 89}, OneDimensionalArrays.bubbleSort(new int[]{5, 3, 7, 12, 11, 89, 1}));
    }

    @Test
    public void testBubbleSortWithOneElement() {
        assertArrayEquals(new int[]{1}, OneDimensionalArrays.bubbleSort(new int[]{1}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBubbleSortWithNoElement() {
        OneDimensionalArrays.bubbleSort(new int[0]);
    }

    //============================================
    //============= selectionSort ================
    //============================================

    @Test
    public void testSelectionSort() {
        assertArrayEquals(new int[]{1, 3, 5, 7, 11, 12, 89}, OneDimensionalArrays.selectionSort(new int[]{5, 3, 7, 12, 11, 89, 1}));
    }

    @Test
    public void testSelectionSortWithOneElement() {
        assertArrayEquals(new int[]{1}, OneDimensionalArrays.selectionSort(new int[]{1}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSelectionSortWithNoElement() {
        OneDimensionalArrays.selectionSort(new int[0]);
    }

    //============================================
    //============= insertionSort ================
    //============================================

    @Test
    public void testInsertionSort() {
        assertArrayEquals(new int[]{1, 3, 5, 7, 11, 12, 89}, OneDimensionalArrays.insertionSort(new int[]{5, 3, 7, 12, 11, 89, 1}));
    }

    @Test
    public void testInsertionSortWithOneElement() {
        assertArrayEquals(new int[]{1}, OneDimensionalArrays.insertionSort(new int[]{1}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInsertionSortWithNoElement() {
        OneDimensionalArrays.insertionSort(new int[0]);
    }
}
