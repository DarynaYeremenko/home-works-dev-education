package com.github.oldHomeWork.main;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Functions {

    private static final String[] BELOW_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
    private static final String[] TENS = {"сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    private static final String[] HUNDREDS = {"тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};

    public static String dayOfWeek(int day) {
        return switch (day) {
            case 1 -> "Понедельник";
            case 2 -> "Вторник";
            case 3 -> "Среда";
            case 4 -> "Четверг";
            case 5 -> "Пятница";
            case 6 -> "Суббота";
            case 7 -> "Воскресенье";
            default -> throw new IndexOutOfBoundsException();
        };
    }

    public static String writeNumber(int number) throws ArrayIndexOutOfBoundsException {
        String soFar;

        if (number % 100 < 20 && number != 100) {
            soFar = BELOW_TWENTY[number % 100];
            number /= 100;
        } else {
            if (number == 100) {
                return (TENS[0]);
            }
            soFar = BELOW_TWENTY[number % 10];
            number /= 10;

            soFar = TENS[number % 10] + " " + soFar;
            number /= 10;
        }
        if (number == 0) return soFar;
        return HUNDREDS[number] + " " + soFar;
    }

    public static int getNumber(String number) throws NullPointerException {
        int result = 0;
        Map<String, Integer> unitsMap = Stream.of(new Object[][]{
                {"ноль", 0},
                {"один", 1},
                {"два", 2},
                {"три", 3},
                {"четыре", 4},
                {"пять", 5},
                {"шесть", 6},
                {"семь", 7},
                {"восемь", 8},
                {"девять", 9},
                {"десять", 10},
                {"одиннадцать", 11},
                {"двенадцать", 12},
                {"тринадцать", 13},
                {"четырнадцать", 14},
                {"пятнадцать", 15},
                {"шестнадцать", 16},
                {"семнадцать", 17},
                {"восемнадцать", 18},
                {"девятнадцать", 19},
                {"двадцать", 20},
                {"тридцать", 30},
                {"сорок", 40},
                {"пятьдесят", 50},
                {"шестьдесят", 60},
                {"семьдесят", 70},
                {"восемьдесят", 80},
                {"девяносто", 90},
                {"сто", 100},
                {"двести", 200},
                {"триста", 300},
                {"четыреста", 400},
                {"пятьсот", 500},
                {"шестьсот", 600},
                {"семьсот", 700},
                {"восемьсот", 800},
                {"девятьсот", 900}
        }).collect(Collectors.toMap(data -> (String) data[0], data -> (Integer) data[1]));

        String[] listOfNumber = number.toLowerCase().split(" ");
        for (String i : listOfNumber) {
            result += unitsMap.get(i);
        }
        return result;
    }

    public static String cartesianDistance(int x1, int y1, int x2, int y2) {
        float result = (float) Math.sqrt((Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2)));
        return result > 0.001 ? String.format("%.2f", result) : "0";
    }
}
