package com.github.oldHomeWork.main;

import java.util.Scanner;

import static java.lang.String.format;

public class ConditionalOperators {

    public static int multiplyEvenNumber(int a, int b) {
        return a % 2 == 0 ? a * b : a + b;
    }

    public static String quarterDefine(int x, int y) {
        return x > 0 && y > 0 ? "Первая четверть" :
                (x > 0 && y < 0 ? "4 четверть" :
                        (x < 0 && y < 0 ? "3 четверть" :
                                (x < 0 && y > 0 ? "2 четверть" : "нельзя определить четверть для указанных координат")));
    }

    public static int positiveNumbersSum(int a, int b, int c) {
        if (a <= 0 && b <= 0 && c <= 0) throw new IllegalArgumentException("Нет положительных чисел");
        return getSumTwoNumbers(getSumTwoNumbers(a, b), c);
    }

    private static int getSumTwoNumbers(int a, int b) {
        return a > 0 && b > 0 ? a + b : (a > 0 ? a : (Math.max(b, 0)));
    }

    public static int maxSumOrMultiplicationMax(int a, int b, int c) {
        return a + b + c > a * b * c ? a + b + c + 3 : (a * b * c) + 3;
    }

    public static String defineRating(int rating) {
        String result = null;
        if (rating < 0 || rating > 100) throw new IllegalArgumentException("Рейтинг некорректный");
        if (rating < 20) result = "F";
        else if (rating < 40) result = "E";
        else if (rating < 60) result = "D";
        else if (rating < 75) result = "C";
        else if (rating < 90) result = "B";
        else if (rating < 100) result = "A";
        return result;
    }
}
