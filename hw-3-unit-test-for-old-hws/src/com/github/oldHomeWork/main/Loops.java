package com.github.oldHomeWork.main;

public class Loops {

    public static int[] sumEvenNumbersAndCount() {
        int sum = 0;
        int counter = 0;
        for (int i = 0; i < 99; i += 2) {
            sum += i;
            counter++;
        }
        return new int[]{sum, counter};
    }

    public static boolean isPrimeNumber(int number) {
        if (number == 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static int sqrtSequential(int number) {
        if (number == 0 || number == 1) {
            return number;
        }
        if (number < 0) {
            throw new IllegalArgumentException();
        }
        int i = 1;
        while (i * i <= number) {
            i++;
        }
        return i - 1;
    }

    public static int sqrtBinary(int num) {
        if (num < 0) {
            throw new IllegalArgumentException();
        }
        if (num == 0 || num == 1) {
            return num;
        }
        int start = 1, end = num / 2;
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (mid == num / mid) {
                return mid;
            }
            if (mid < num / mid) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }

        return start - 1;
    }

    public static int findFactorial(int num) {
        if (num < 0) {
            throw new IllegalArgumentException();
        }
        if (num == 0) {
            return num;
        }
        int factorial = 1;
        if (num > 1) {
            for (int i = 2; i <= num; i++) {
                factorial *= i;
            }
        }
        return factorial;
    }

    public static int sumNumbersOfValue(int number) {
        if (number < 0) {
            throw new IllegalArgumentException();
        }
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

    public static int mirrorSequence(int number) {
        int reverseNum = 0;
        while (number > 0) {
            reverseNum = (reverseNum + (number % 10)) * 10;
            number = number / 10;
        }
        return reverseNum / 10;
    }
}
