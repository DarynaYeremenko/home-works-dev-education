package com.github.oldHomeWork.main;

public class OneDimensionalArrays {

    public static int arrayMin(int[] array) throws ArrayIndexOutOfBoundsException {
        int response = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < response) {
                response = array[i];
            }
        }
        return response;
    }

    public static int arrayMax(int[] array) throws ArrayIndexOutOfBoundsException {
        int response = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > response) {
                response = array[i];
            }
        }
        return response;
    }

    public static int getIndexOfArrayMin(int[] array) throws ArrayIndexOutOfBoundsException {
        int response = 0, minValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                response = i;
                minValue = array[i];
            }
        }
        return response;
    }

    public static int getIndexOfArrayMax(int[] array) throws ArrayIndexOutOfBoundsException {
        int response = 0, maxValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxValue) {
                response = i;
                maxValue = array[i];
            }
        }
        return response;
    }

    public static int sumOfArrayOdd(int[] array) {
        int response = 0;
        for (int i = 1; i < array.length; i++) {
            if (i % 2 != 0) {
                response += array[i];
            }
        }
        return response;
    }

    public static int[] arrayRevers(int[] array) {
        int sizeArray = array.length;
        if (sizeArray == 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int[] response = new int[sizeArray];
        int currentIndex = sizeArray - 1;
        for (int i = 0; i <= (sizeArray - 1); currentIndex--, i++) {
            response[currentIndex] = array[i];
        }
        return response;
    }

    public static int countOfArrayOddElements(int[] array) {
        int response = 0;
        for (int currentElement : array) {
            if (currentElement % 2 != 0) {
                response++;
            }
        }
        return response;
    }

    public static int[] replaceTwoPartOfArray(int[] array) {
        if (array.length <= 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int half = array.length / 2;
        int div = half + array.length % 2;
        for (int i = 0; i < div; i++) {
            int currentElement = array[i];
            array[i] = array[div + i];
            array[div + i] = currentElement;
        }
        return array;
    }

    public static int[] bubbleSort(int[] array) {
        if (array.length == 0) {
            throw new IllegalArgumentException();
        }
        if (array.length == 1) {
            return array;
        } else {
            for (int lengthIn = array.length; lengthIn > 0; lengthIn--) {
                for (int currSwap = 0; currSwap < lengthIn - 1; currSwap++) {
                    if (array[currSwap] > array[currSwap + 1]) {
                        int curElement = array[currSwap];
                        array[currSwap] = array[currSwap + 1];
                        array[currSwap + 1] = curElement;
                    }
                }
            }
        }
        return array;
    }

    public static int[] selectionSort(int[] array) {
        int min;
        if (array.length == 0) {
            throw new IllegalArgumentException();
        }
        if (array.length == 1) {
            return array;
        } else {
            for (int OutIterations = 0; OutIterations < array.length - 1; OutIterations++) {
                min = OutIterations;
                for (int CountInIterations = OutIterations; CountInIterations < array.length; CountInIterations++) {
                    if (array[CountInIterations] < array[min]) {
                        min = CountInIterations;
                    }
                }
                int temp = array[OutIterations];
                array[OutIterations] = array[min];
                array[min] = temp;
            }
        }
        return array;
    }

    public static int[] insertionSort(int[] array) {
        if (array.length == 0) {
            throw new IllegalArgumentException();
        }
        if (array.length == 1) {
            return array;
        } else {
            int j, value;

            for (int i = 1; i < array.length; i++) {
                value = array[i];
                for (j = i - 1; j >= 0 && array[j] > value; j--) {
                    array[j + 1] = array[j];
                }
                array[j + 1] = value;
            }
        }
        return array;
    }
}
