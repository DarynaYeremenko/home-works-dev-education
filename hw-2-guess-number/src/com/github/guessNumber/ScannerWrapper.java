package com.github.guessNumber;

import java.util.Scanner;

public class ScannerWrapper {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static int getNumber() {
        return SCANNER.nextInt();
    }

    public static String getString() {
        return SCANNER.next();
    }

    public static boolean isNumber(){
        return SCANNER.hasNextInt();
    }

    public static boolean isString(){
        return  SCANNER.hasNext();
    }
}


