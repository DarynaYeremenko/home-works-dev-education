package com.github.guessNumber;

public class Game {

    private static int guessNumberProcess(int secretNumber, int firstAttempt, int tries) {
        int guess;
        int count = 0;
        do {
            if (count == 0) {
                guess = firstAttempt;
            } else if (count < tries) {
                guess = ScannerWrapper.getNumber();
            } else {
                System.out.println("Time is out!");
                return 0;
            }
            count++;
            if (guess == secretNumber) {
                System.out.println("Your guess is correct. Congratulations!");
                return guess;
            } else if (guess < secretNumber) {
                System.out.println(String.format("Your guess is smaller than the secret number. You have %s attempts", tries - count));
            } else
                System.out.println(String.format("Your guess is greater than the secret number. You have %s attempts", tries - count));
        } while (true);
    }

    public static void startGame(int min, int max, int tries) {
        SecretNumber secretNumber = new SecretNumber(new GuessNumberValidation(min, max, tries));
        try {

            String welcomeMessage = "Welcome! Try to guess the secret number\n" +
                    String.format("Think about a number from %s - %s range and will try to guess it in %s attempts\n", min, max, tries) +
                    "To quit at anytime please type \"exit\". Enter point:";
            System.out.println(welcomeMessage);

            while (ScannerWrapper.isString()) {
                if (ScannerWrapper.isNumber()) {
                    int result = guessNumberProcess(secretNumber.getSecretNumber(), ScannerWrapper.getNumber(), tries);
                    if (result == 0 || result == secretNumber.getSecretNumber())
                        break;
                } else {
                    String input = ScannerWrapper.getString();
                    if (input.equalsIgnoreCase("exit")) {
                        System.out.println("Exiting");
                        break;
                    } else {
                        System.out.println("You did not enter a valid value. Please enter a number or \"exit\" to quit.");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Exiting Program.");

        }
    }
}
