package com.github.guessNumber;

public class GuessNumberValidation {

    private final int min;
    private final int max;
    private final int attempt;

    public GuessNumberValidation(int min, int max, int attempt) {
        if (min <= 0 || max <= 0 || attempt <= 0) {
            throw new IllegalArgumentException("The number must be greater than 1!");
        }
        if (min > 200 || max > 200) {
            throw new IllegalArgumentException("The number must be less than 200");
        }
        if (attempt > 15) {
            throw new IllegalArgumentException("The count of attempt must be less than 15");
        } else {
            this.min = min;
            this.max = max;
            this.attempt = attempt;
        }
    }

    public int getAttempt() {
        return attempt;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }


}
