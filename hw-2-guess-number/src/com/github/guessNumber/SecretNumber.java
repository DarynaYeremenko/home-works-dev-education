package com.github.guessNumber;

public class SecretNumber {

    private static int secretNumber;

    SecretNumber(GuessNumberValidation guessNumberValidation){
        secretNumber = (int) (Math.random() * (guessNumberValidation.getMax() - guessNumberValidation.getMin() + 1) + guessNumberValidation.getMin());
    }

    public int getSecretNumber() {
        return secretNumber;
    }
}
