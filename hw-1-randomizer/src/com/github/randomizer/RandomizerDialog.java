package com.github.randomizer;

import java.util.ArrayList;

import static com.github.randomizer.RandomArray.createRandomArray;

public class RandomizerDialog {
    public static void randomizerDialog(int a, int b) {
        ArrayList<Integer> randomNumbers = createRandomArray(a, b);
        int count = 0;
        while (true){
            try {
                String command = ScannerWrapper.getCommand();
                switch (command) {
                    case "generate":
                        try {
                            System.out.println(String.format("Unique random number: %s", randomNumbers.get(count)));
                            System.out.println(count);
                            count++;
                            break;
                        } catch (IndexOutOfBoundsException e) {
                            System.out.println("This command cannot be used because there are no more unique values");
                            break;
                        }
                    case "help":
                        System.out.println("The GENERATE command gives a random unique number in the selected range");
                        break;
                    case "exit":
                        System.out.println("App is not active");
                        System.exit(0);
                }
            } catch (IllegalArgumentException e) {
                System.out.println("PLEASE, input correct command!");
            }
        }
    }

}
