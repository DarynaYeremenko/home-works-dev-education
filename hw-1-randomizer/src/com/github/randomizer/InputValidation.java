package com.github.randomizer;


public class InputValidation {
    static int min;
    static int max;

    public static int inputCorrectNumber(String type) {
        int number;
        do {
            System.out.println("Please enter a positive number less than 500 to be used as " + type);
            while (!ScannerWrapper.isNumberExist()) {
                System.out.println("That's not an integer!");
                ScannerWrapper.getCommand();
            }
            number = ScannerWrapper.getNumber();
        } while (number <= 0 || number > 500);
        return number;
    }

    public static void swapTwoNumber() {
        int temp;
        temp = min;
        min = max;
        max = temp;
    }

    public static void createRangeNumber() {
        min = inputCorrectNumber("MIN");
        max = inputCorrectNumber("MAX");
        if (min > max)
            swapTwoNumber();
    }
}
