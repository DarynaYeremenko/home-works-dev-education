package com.github.randomizer;

import java.util.ArrayList;
import java.util.Collections;

public class RandomArray {

    public static ArrayList<Integer> createRandomArray(int min, int max) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        return list;
    }
}
