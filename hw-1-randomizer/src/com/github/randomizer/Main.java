package com.github.randomizer;

public class Main {


    public static void main(String[] args) {
        InputValidation.createRangeNumber();
        System.out.println(String.format("[MIN,MAX] = [%s, %s]", InputValidation.min, InputValidation.max));
        System.out.println("Input command: (generate, exit, help)");
        RandomizerDialog.randomizerDialog(InputValidation.min, InputValidation.max);
    }
}
